package com.crossover.techtrial.controller;

import com.crossover.techtrial.model.HourlyElectricity;
import com.crossover.techtrial.model.Panel;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import org.springframework.test.web.servlet.setup.MockMvcBuilders;


/**
 * PanelControllerTest class will test all APIs in PanelController.java.
 * @author Crossover
 *
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class PanelControllerTest {
  
  MockMvc mockMvc;
  
  @Mock
  private PanelController panelController;
  
  @Autowired
  private TestRestTemplate template;

  @Before
  public void setup() throws Exception {
    mockMvc = MockMvcBuilders.standaloneSetup(panelController).build();
  }

  @Test
  public void testPanelShouldBeRegistered() throws Exception {
    HttpEntity<Object> panel = getHttpEntity(
        "{\"serial\": \"2323239087654667\", \"longitude\": \"54.123232\"," 
            + " \"latitude\": \"54.123232\",\"brand\":\"tesla\" }");
    ResponseEntity<Panel> response = template.postForEntity(
        "/api/register", panel, Panel.class);
    Assert.assertEquals(202,response.getStatusCode().value());
  }
  @Test
  public void testPanelShouldNotBeRegisteredForPanelSerialLength() throws Exception {
    HttpEntity<Object> panel = getHttpEntity(
        "{\"serial\": \"23232390876\", \"longitude\": \"54.123232\"," 
            + " \"latitude\": \"54.123232\",\"brand\":\"tesla\" }");
    ResponseEntity<Panel> response = template.postForEntity(
        "/api/register", panel, Panel.class);
    Assert.assertEquals(400,response.getStatusCode().value());
  }
  @Test
  public void testPanelShouldNotBeRegisteredForExistingPanelSerial() throws Exception {
    HttpEntity<Object> panel = getHttpEntity(
        "{\"serial\": \"2323239087654667\", \"longitude\": \"54.123232\"," 
            + " \"latitude\": \"54.123232\",\"brand\":\"tesla\" }");
    ResponseEntity<String> response = template.postForEntity(
        "/api/register", "", String.class);
    Assert.assertEquals(400,response.getStatusCode().value());
  }
  @Test
  public void testSaveHourlyElectricity() throws Exception {
    HttpEntity<Object> hourlyElectricity = getHttpEntity(
    		"{\"generatedElectricity\": 34 ,\"readingAt\": \"2018-07-27T16:36:22\"}"); 
    ResponseEntity<HourlyElectricity> response = template.postForEntity(
        "/api/panels/2323239087654667/hourly", hourlyElectricity, HourlyElectricity.class);
    Assert.assertEquals(200,response.getStatusCode().value());
  }
  @Test
  public void testHourlyElectricityShouldNotBeSavedForInvalidPanelSerial() throws Exception {
    HttpEntity<Object> hourlyElectricity = getHttpEntity(
        "{\"generatedElectricity\": 34 ,\"readingAt\": \"2018-07-27T16:36:22\"}"); 
    ResponseEntity<String> response = template.postForEntity(
        "/api/panels/2323239087/hourly", "", String.class);
    Assert.assertEquals(400,response.getStatusCode().value());
  }
  @Test
  public void testGetHourlyElectricity() throws Exception {
    ResponseEntity<String> response = template.getForEntity(
        "/api/panels/2323239087654667/hourly", String.class);
    Assert.assertEquals(200,response.getStatusCode().value());
  }
  @Test
  public void testGetDailyElectricity() throws Exception {
    ResponseEntity<String> response = template.getForEntity(
        "/api/panels/2323239087654667/daily", String.class);
    Assert.assertEquals(200,response.getStatusCode().value());
  }
  private HttpEntity<Object> getHttpEntity(Object body) {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    return new HttpEntity<Object>(body, headers);
  }
}
